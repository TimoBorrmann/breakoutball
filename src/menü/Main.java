package menü;

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main extends Application {

    private Parent createContent() {

        Pane root = new Pane();

        root.setPrefSize(700, 600);

        try (InputStream is = Files.newInputStream(Paths.get("C:\\Users\\timob\\OneDrive\\Desktop\\png-homer-5.png"))) {

            ImageView img = new ImageView(new Image(is));
            img.setFitWidth(700);
            img.setFitHeight(600);
            root.getChildren().add(img);

        } catch (IOException e) {
            System.out.println("Couldn't load image");
        }

        Title title = new Title("BreakoutBall");
        title.setTranslateX(240);
        title.setTranslateY(40);


        MenüBox vbox = new MenüBox(
                new MenüItem("Start"),
                new MenüItem("Exit"));


        vbox.setTranslateX(480);
        vbox.setTranslateY(400);

        root.getChildren().addAll(title, vbox);
        return root;

    }

    @Override
    public void start(Stage primaryStage) throws Exception {


        Scene scene = new Scene(createContent());
        primaryStage.setTitle("BreakoutBall");
        primaryStage.setScene(scene);
        primaryStage.show();

    }
    public static void main(String[] args) {
        launch(args);

    }

}




