package menü;

import javafx.geometry.Pos;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class Title extends StackPane {
    public Title(String name) {

        Rectangle bg = new Rectangle(450, 60);
        bg.setStroke(Color.BLACK);
        bg.setStrokeWidth(3);
        bg.setFill(Color.CYAN);


        Text text = new Text(name);
        text.setFill(Color.BLACK);
        text.setFont(Font.font("Serif", FontWeight.SEMI_BOLD, 50));


        setAlignment(Pos.TOP_CENTER);
        getChildren().addAll(bg,text);

    }

}

