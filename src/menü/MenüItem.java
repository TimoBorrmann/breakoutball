package menü;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class MenüItem extends StackPane {

    public MenüItem(String name) {

        LinearGradient gradient = new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, new Stop[] {

                new Stop(0, Color.YELLOW),

                new Stop(0.1, Color.BLACK),

                new Stop(0.9, Color.BLACK),

                new Stop(1, Color.YELLOW)



        });

        Rectangle bg = new Rectangle(200,60);
        bg.setStroke(Color.BLACK);
        bg.setStrokeWidth(5);
        bg.setOpacity(0.4);


        Text text = new Text(name);
        text.setFill(Color.BLACK);
        text.setFont(Font.font("Serif", FontWeight.SEMI_BOLD,40));

//        Button start = new Button ("Start");
//        start.setTextFill(Color.TRANSPARENT);


        setAlignment(Pos.CENTER);
        getChildren().addAll(bg, text);


        setOnMouseEntered(event -> {
            bg.setFill(gradient);
            text.setFill(Color.WHITE);

        });

        setOnMouseExited(event -> {
            bg.setFill(Color.BLACK);
            text.setFill(Color.BLACK);
        });

        setOnMousePressed(event -> {
            bg.setFill(Color.YELLOW);

        });

        setOnMouseReleased(event -> {
            bg.setFill(gradient);
        });

    }

}



