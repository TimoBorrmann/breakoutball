package menü;


import javafx.scene.layout.VBox;
import javafx.scene.shape.Line;


public class MenüBox extends VBox {

    public MenüBox(MenüItem...items) {

        getChildren().add(createSeperator());

        for (MenüItem item : items) {
            getChildren().addAll(item,createSeperator());
        }
    }

    private Line createSeperator() {

        Line sep = new Line();
        sep.getEndX();
        return sep;
    }
}

