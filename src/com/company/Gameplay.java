package com.company;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.Timer;


import javax.swing.JPanel;

public class Gameplay extends JPanel implements KeyListener, ActionListener {
    private boolean play = false;
    private int score = 0;

    private int totalBricks = 84;

    private Timer timer;
    private int delay = 4;

    private int playerX = 310;

    private int ballposX = 220; // Startposition Ball
    private int ballposY = 310; // Startposition Ball

    private int ballspeed = -2;
    private int ballXdir = ballspeed;
    private int ballYdir = ballspeed*2;

    private MapGenerator map;

    public Gameplay() {
        map = new MapGenerator(6,14); // Anzahl der Bricks 6*14
        addKeyListener(this);
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
        timer = new Timer(delay, this);
        timer.start();
    }

    public void paint(Graphics g) {
        //background
        g.setColor(Color.BLACK);
        g.fillRect(1, 1, 692, 592);

        // drawing map
        map.draw((Graphics2D)g);

        // borders
        g.setColor(Color.CYAN);
        g.fillRect(0, 0, 3, 592);
        g.fillRect(0, 0, 692, 3);
        g.fillRect(691, 0, 3, 592);

        // scores
        g.setColor(Color.WHITE);
        g.setFont(new Font("serif", Font.BOLD, 25));
        g.drawString("" + score, 590,30 );

        // the paddle
        g.setColor(Color.CYAN);
        g.fillRect(playerX, 550, 100, 20);

        //the ball
        g.setColor(Color.WHITE);
        g.fillOval(ballposX, ballposY, 20, 20);

        // Funktion wenn man alle Bricks abgeräumt hat.
        if(totalBricks <= 0){
            play = false;
            ballXdir = 0;
            ballYdir = 0;
            g.setColor(Color.YELLOW);
            g.setFont(new Font("serfi", Font.BOLD, 30));
            g.drawString("Glückwunsch, du hast gewonnen!!!" + " Dein Score "+ score, 120,300);

        }

        if(ballposY > 570){
            play = false;
            ballXdir = 0;
            ballYdir = 0;
            g.setColor(Color.RED);
            g.setFont(new Font("serfi", Font.BOLD, 30));
            g.drawString("Game Over" + " Dein Score:" + score, 110,300);
            g.setFont(new Font("serfi", Font.BOLD, 20));
            g.drawString("Restart? Drücke Enter", 230,350);


        }

        g.dispose();


    }

    @Override
    public void actionPerformed(ActionEvent e) {
        timer.start();
        if(play) {
            if(new Rectangle(ballposX,ballposY,20,20).intersects(new Rectangle(playerX,550,100,8))){
                ballYdir = -ballYdir;
            }

            // Danke Google :-)
          A:  for(int i=0; i <map.map.length; i++){
                for(int j = 0; j <map.map[0].length; j++){
                    if(map.map[i][j] > 0){
                        int brickX = j *map.brickWitdh+80;
                        int brickY = i * map.brickHeight+50;
                        int brickWidth = map.brickWitdh;
                        int brickHeight = map.brickHeight;

                        Rectangle rect = new Rectangle(brickX,brickY,brickWidth,brickHeight);
                        Rectangle ballRect = new Rectangle(ballposX, ballposY, 20,20);
                        Rectangle brickRect = rect;

                        if(ballRect.intersects(brickRect)){
                            map.setBrickValue(0,i,j);
                            totalBricks--;
                            score+=10;

                            if(ballposX +19 <= brickRect.x || ballposX +1 >= brickRect.x + brickRect.width){
                                ballXdir = -ballXdir;
                            }else {
                                ballYdir = -ballYdir;
                            }
                            break A;
                        }
                    }
                }
            }

            ballposX += ballXdir;
            ballposY += ballYdir;
            if(ballposX < 0){           // left border
                ballXdir = -ballXdir;
            }
            if(ballposY < 0){
                ballYdir = -ballYdir;
            }
            if(ballposX > 670){         // right boarder
                ballXdir = -ballXdir;
            }
        }

        repaint();

    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            if (playerX >= 600) {
                playerX = 600;
            } else {
                moveRight();
            }
        }
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            if (playerX < 10) {
                playerX = 10;
            } else {
                moveLeft();
            }
        }   // Zum Restart
        if (e.getKeyCode() == KeyEvent.VK_ENTER){
            if(!play){
                play = true;
                ballposX = 220;
                ballposY = 350;
                ballXdir = ballspeed;
                ballYdir = ballspeed*2;
                playerX = 310;
                score = 0;
                delay = 6;
                totalBricks = 32;
                map = new MapGenerator(4,8); // Neuer Zustand hat sich verringert

                repaint();
            }
        }
    }

    public void moveRight() {
        play = true;
        playerX += 40;
    }

    public void moveLeft() {
        play = true;
        playerX -= 40;
    }


}



