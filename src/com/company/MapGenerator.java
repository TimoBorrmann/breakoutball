package com.company;


import java.awt.*;

public class MapGenerator {
    public int map[][];
    public int brickWitdh;
    public int brickHeight;

    public MapGenerator(int row, int col) {
        map = new int[row][col];
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[0].length; j++) {
                map[i][j] = 1;
            }
        }

        brickWitdh = 540/col;
        brickHeight = 150/row;
    }
    public void draw(Graphics2D g){
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[0].length; j++) {
                if(map[i][j] > 0){
                    g.setColor(Color.YELLOW);
                    g.fillRect(j*brickWitdh + 80, i * brickHeight +50,brickWitdh,brickHeight);

                    g.setStroke(new BasicStroke(3));
                    g.setColor(Color.BLACK);
                    g.drawRect(j*brickWitdh + 80, i * brickHeight +50,brickWitdh,brickHeight);
                }

            }
        }
    }
    public void setBrickValue(int value, int row, int col){
        map[row][col] = value;

    }
}
